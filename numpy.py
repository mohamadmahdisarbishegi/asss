#!/usr/bin/env python
# coding: utf-8

# In[9]:


import numpy as np
array =np.array( [np.arange(10 , 20 ) , np.arange(20 , 30 )])
array[::-1][:]


# In[6]:


array2 = np.array([1,2,3,1,2,3,1,2,3])
A = array2 < 3
#A = np.array([1.5,2,3,1,2,3,1,2,3])
A = A.astype(int)


# In[7]:


A


# In[8]:


np.linalg.norm(array2)


# In[25]:


array3 = np.random.rand(2,3)*12
array4 = np.random.rand(2,3)*6+4
print(array3 ,2* '\n' , array4)
np.maximum(array3 , array4)


# In[27]:


np.rint(array4)


# In[33]:


def one_hot_encoding(data , alphabet):
    char_to_int = dict((c, i) for i, c in enumerate(alphabet))    
    integer_encoded = [char_to_int[char] for char in data]    
    onehot_encoded = np.array([[0 , 0 , 0 , 0]])
    for value in integer_encoded:
        letter = [0 for _ in range(len(alphabet))]
        letter[value] = 1
        onehot_encoded = np.append(onehot_encoded , [letter] , axis = 0 )
    return(onehot_encoded)


# In[35]:


one_hot_encoding('ATAAACTACGGGTACGTTAGCT' , 'ATCG')


# In[3]:


List = [1,2,3,5,6]
print(List[1:3])
import numpy as np

print(np.random.permutation(10))


# In[4]:


int(1.23)

